﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TextAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            #endregion

            program.ModeSelection();
        }

        /// <summary>
        /// Method for all error handling.
        /// </summary>
        /// <param name="message">Error message to display.</param>
        /// <param name="exit">Bool value to close program for fatal errors.</param>
        public void Error(string message, bool exit)
        {
            Console.WriteLine("Error: {0}", message);
            Console.ReadLine();
            Console.Clear();
            if (exit)
                Environment.Exit(0);
        }

        /// <summary>
        /// A menu to select which mode to enter.
        /// </summary>
        public void ModeSelection()
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            #endregion

            Console.Clear();
            Console.WriteLine("Would you like to input or identify: ");
            string answer = Console.ReadLine();

            if (String.Compare(answer.ToLower(), "input") == 0)
            {
                input.Menu();
            }
            else if (String.Compare(answer.ToLower(), "identify") == 0)
            {
                identify.Menu();
            }
            else
            {
                program.Error("must enter 'input' or 'identify', press enter to retry.", false);
                program.ModeSelection();
            }
        }
    }
}
