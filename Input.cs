﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace TextAnalysis
{
    class Input 
    {
        /// <summary>
        /// Menu method for input mode.
        /// </summary>
        public void Menu()
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            Analysis analyze = new TextAnalysis.Analysis();
            #endregion

            Console.Clear();
            Console.WriteLine("You selected input.");
            Console.WriteLine("Enter author of text.");
            string author = Console.ReadLine().ToLower();
            Console.WriteLine("Enter file to read from.");
            string file = Console.ReadLine();

            if (File.Exists(file))
            {
                XMLWrite(author, file);
            }
            else
            {
                program.Error("File does not exist.", true);
            }

            Console.Clear();
            Console.WriteLine("Would you like to input, identify, or exit?");
            string answer = Console.ReadLine();
            
            if (answer == "input")
            {
                input.Menu();
            }
            else if (answer == "identify")
            {
                identify.Menu();
            }
            else 
            {
                Environment.Exit(0);
            }

        }

        /// <summary>
        /// Writes text statistics to XML file.
        /// </summary>
        /// <param name="author">Author of the text.</param>
        /// <param name="file">XML file to write to.</param>
        public void XMLWrite(string author, string file)
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            Analysis analyze = new TextAnalysis.Analysis();
            #endregion

            #region Statistics Variables
            float chars = analyze.Chars(file);
            float words = analyze.Words(file);
            float sentences = analyze.Sentences(file);
            float paragraphs = analyze.Paragraphs(file);
            float average_chars = chars / words;
            float average_words = words / sentences;
            float average_sentences = sentences / paragraphs;
            float text_score = average_chars + average_words + average_sentences;
            #endregion

            XDocument samplesXml;
            XElement rootElem;

            if (File.Exists("samples.xml"))
            {
                samplesXml = XDocument.Load("samples.xml");
                rootElem = samplesXml.Root;
            }
            else
            {
                samplesXml = new XDocument();
                rootElem = new XElement("Samples");
                samplesXml.Add(rootElem);
            }

            var authorElem = new XElement("Sample");
            
            #region authorElem Attributes
            var nameAttr = new XAttribute("Name", author);
            var fileAttr = new XAttribute("File", file);
            var averageCharsAttr = new XAttribute("AverageChars", average_chars);
            var averageWordsAttr = new XAttribute("AverageWords", average_words);
            var averageSentencesAttr = new XAttribute("AverageSentences", average_sentences);
            var charsAttr = new XAttribute("Chars", chars);
            var wordsAttr = new XAttribute("Words", words);
            var sentencesAttr = new XAttribute("Sentences", sentences);
            var paragraphsAttr = new XAttribute("Paragraphs", paragraphs);
            var textScoreAttr = new XAttribute("TextScore", text_score);
            #endregion

            authorElem.Add(nameAttr, fileAttr, averageCharsAttr, averageWordsAttr, averageSentencesAttr, charsAttr, wordsAttr, sentencesAttr, paragraphsAttr, textScoreAttr);
            rootElem.Add(authorElem);
            samplesXml.Save("samples.xml");
        }
    }
}
