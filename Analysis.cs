﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TextAnalysis
{
    public class Analysis
    {
        /// <summary>
        /// Determines # of sentences in text file.
        /// </summary>
        /// <param name="file">Name of text file.</param>
        /// <returns># of sentences.</returns>
        public float Sentences(string file)
        {
            char[] text = (File.ReadAllText(file)).ToCharArray();
            int sentencecount = 0;

            foreach (char letter in text)
            {
                if (letter == '.' || letter == '!')
                {
                    sentencecount += 1;
                }
            }
            return sentencecount;
        }

        /// <summary>
        /// Returns # of words in text file.
        /// </summary>
        /// <param name="file">Name of text file.</param>
        /// <returns># of words.</returns>
        public float Words(string file)
        {
            char[] text = (File.ReadAllText(file)).ToCharArray();
            int wordcount = 0;
            bool new_word = true;

            foreach (char letter in text)
            {
                if (letter != ' '  && letter != '\n' && letter != '\r')
                {
                    if (new_word == true)
                    {
                        wordcount += 1;
                        new_word = false;
                    }
                }

                if (letter == ' ' || letter == '\n')
                {
                    new_word = true; 
                }
            }
            return wordcount;
        }

        /// <summary>
        /// Determines # of chars in text file.
        /// </summary>
        /// <param name="file">Name of text file.</param>
        /// <returns># of chars.</returns>
        public float Chars(string file)
        {
            char[] text = (File.ReadAllText(file)).ToCharArray();
            int charcount = 0;

            foreach (char letter in text)
            {
                if (letter != ' ' && letter != '\n' && letter != '\r')
                {
                    charcount += 1;
                }
            }
            return charcount;
        }

        /// <summary>
        /// Determines # of paragraphs in text file.
        /// </summary>
        /// <param name="file">Name of text file.</param>
        /// <returns># of paragraphs.</returns>
        public float Paragraphs(string file)
        {
            char[] text = (File.ReadAllText(file)).ToCharArray();
            int paragraphcount = 1;
            bool newparagraph = false;

            foreach (char letter in text)
            {
                if ((letter == '\n') && (newparagraph))
                {
                    paragraphcount += 1;
                    newparagraph = false;
                }

                if (letter != '\n' && letter != '\r')
                {
                    newparagraph = true;
                }
            }
            return paragraphcount;
        }

        /// <summary>
        /// Prints text analysis variables.
        /// </summary>
        /// <param name="file">Name of file.</param>
        public void PrintAnalysis(string file)
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            Analysis analyze = new TextAnalysis.Analysis();
            #endregion

            #region Variables
            float chars = analyze.Chars(file);
            float words = analyze.Words(file);
            float sentences = analyze.Sentences(file);
            float average_chars = chars / words;
            float average_words = words / sentences; 
            #endregion

            Console.WriteLine(chars);
            Console.WriteLine(words);
            Console.WriteLine(sentences);
            Console.WriteLine(average_chars);
            Console.WriteLine(average_words);
        }
    }
}
