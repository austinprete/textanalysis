﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace TextAnalysis
{
    class Identify
    {
        /// <summary>
        /// Menu method for identify mode.
        /// </summary>
        public void Menu()
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            Analysis analyze = new TextAnalysis.Analysis();
            #endregion

            Console.Clear();
            Console.WriteLine("You selected identify.");
            Console.WriteLine("Enter file to read from.");
            string file = Console.ReadLine();
            if (File.Exists(file))
            {
                XMLRead(file);
            }
            else
            {
                program.Error("File does not exist.", true);
            }

            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Would you like to input, identify, or exit?");
            string answer = Console.ReadLine();

            if (answer == "input")
            {
                input.Menu();
            }
            else if (answer == "identify")
            {
                identify.Menu();
            }
            else
            {
                Environment.Exit(0);
            }

        }

        /// <summary>
        /// Read from XML file containing text statistics.  Prints author guess.
        /// </summary>
        /// <param name="file">Name of XML file.</param>
        public void XMLRead(string file)
        {
            #region Object Initializations
            Program program = new TextAnalysis.Program();
            Input input = new TextAnalysis.Input();
            Identify identify = new TextAnalysis.Identify();
            Analysis analyze = new TextAnalysis.Analysis();
            #endregion

            #region Statistics Variables
            float chars = analyze.Chars(file);
            float words = analyze.Words(file);
            float sentences = analyze.Sentences(file);
            float paragraphs = analyze.Paragraphs(file);
            float average_chars = chars / words;
            float average_words = words / sentences;
            float average_sentences = sentences / paragraphs;
            float text_score = average_chars + average_words + average_sentences;
            #endregion

            XDocument samplesXml = new XDocument(); ;
            XElement rootElem = samplesXml.Root;

            if (File.Exists("samples.xml"))
            {
                samplesXml = XDocument.Load("samples.xml");
                rootElem = samplesXml.Root;
            }
            else
            {
                Console.WriteLine("Please input text first.");
                Console.WriteLine("Press enter to continue.");
                Console.ReadLine();
                program.ModeSelection();

            }

            XElement []array = (rootElem.Elements("Sample")).ToArray();
            XAttribute author = array[0].Attribute("Name");
            decimal value = Math.Abs(Convert.ToDecimal(array[0].Attribute("TextScore").Value) - Convert.ToDecimal(text_score));

            
            foreach (XElement i in array)
            {
                if ((Math.Abs(Convert.ToDecimal(i.Attribute("TextScore").Value) - Convert.ToDecimal(text_score))) < value)
                {
                    value = Math.Abs(Convert.ToDecimal(i.Attribute("TextScore").Value) - Convert.ToDecimal(text_score));
                    author = i.Attribute("Name");
                }
            }

            Console.WriteLine(author);

        }
    }
}
